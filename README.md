# Tap Alignment Jigs for Openbuilds-style Extrusion

![Tap Alignment Jigs in printing orientation](tap-alignment-jigs.png)

These jigs are used to quickly tap 2020, 2040, 2060, 2080, 4040,
and C-Beam extrusion.

The smallest jig works for 2020, 2040, 2060, and 2080 (you will
have to invert it when you slice it; I accidentally modeled it with
the tangs pointing down as the slicer sees it; make it match the
picture to print). The larger square jig works for 4040, and the
C-shaped one for C-Beam.

Print them out, snap them in place on the end of the extrusion,
and M5 tap through the holes in the jigs to align with the holes
in the extrusion and get the threads straight from the start.

Spiral flute taps with polished flutes are highly recommended; if
you are using extrusion with undersized holes you might consider
a tap/drill with spiral flutes. Spiral flutes eject the chips out
the top.  The counterbores give a space for the chips to eject into
while you are tapping.

With a spiral flute tap, using alcohol as a lubricant, you should
be able to power tap holes with a power drill, with a clutch set
on a light setting, in a single pass at slow speed.

The FCStd files were created using the "realthunder" Assembly3
branch of FreeCAD 0.19.
